package kit.start.data

import io.reactivex.Single
import io.reactivex.withSchedulers
import kit.start.entities.CodeResponse
import javax.inject.Inject


class RepositoryImpl  @Inject constructor(private val api: Api) : Repository {

    override fun send(qr: String) = api.sendCode(qr).withSchedulers()

    override fun months() = api.stats().withSchedulers()
}
