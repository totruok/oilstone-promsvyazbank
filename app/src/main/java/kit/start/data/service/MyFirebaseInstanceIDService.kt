package kit.start.data.service


import android.util.Log
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.FirebaseInstanceIdService
import com.google.firebase.messaging.FirebaseMessaging
import kit.start.R

class MyFirebaseInstanceIDService : FirebaseInstanceIdService() {

    private val TAG = this.javaClass.simpleName!!

    override fun onTokenRefresh() {
        val refreshedToken = FirebaseInstanceId.getInstance().token
        sendRegistrationToServer(refreshedToken)
    }

    private fun sendRegistrationToServer(token: String?) {
        Log.d(TAG, token)
        FirebaseMessaging.getInstance().subscribeToTopic(applicationContext.getString(R.string.notification_channel_topic_receipt_new))
    }
}