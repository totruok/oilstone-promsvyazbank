package kit.start.data.service

import android.util.Log
import com.firebase.jobdispatcher.JobService

class MyJobService : JobService() {

    private val TAG = this.javaClass.simpleName!!


    override fun onStartJob(job: com.firebase.jobdispatcher.JobParameters?): Boolean {
        Log.d(TAG, "onStartJob")
        return true
    }

    override fun onStopJob(job: com.firebase.jobdispatcher.JobParameters?): Boolean {
        Log.d(TAG, "onStopJob")
        return true
    }

    companion object {

        private val TAG = "MyJobService"
    }

}