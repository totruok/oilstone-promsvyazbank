package kit.start.data

import io.reactivex.Single
import kit.start.entities.CodeResponse
import kit.start.entities.Month


interface Repository {
    fun send(qr: String): Single<CodeResponse>

    fun months(): Single<List<Month>>
}
