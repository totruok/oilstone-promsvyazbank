package kit.start.data

import io.reactivex.Single
import kit.start.entities.CodeResponse
import kit.start.entities.Month
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST


interface Api {

    @FormUrlEncoded
    @POST("/process_bill")
    fun sendCode(@Field("code") code: String): Single<CodeResponse>

    @GET("/get_months_stats")
    fun stats(): Single<List<Month>>
}
