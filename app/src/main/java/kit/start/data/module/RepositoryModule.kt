package kit.start.data.module

import dagger.Module
import dagger.Provides
import kit.start.data.Api
import kit.start.data.Repository
import kit.start.data.RepositoryImpl
import javax.inject.Singleton

@Module
class RepositoryModule {

    @Provides
    @Singleton
    fun provideRepository(api: Api): Repository {
        return RepositoryImpl(api)

    }
}
