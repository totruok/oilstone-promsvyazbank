package kit.start.data

import android.app.Application
import dagger.Component
import kit.start.data.module.ApiModule
import kit.start.data.module.AppModule
import kit.start.data.module.NetworkModule
import kit.start.data.module.RepositoryModule
import kit.start.presenter.controller.AnalyticsMonthController
import kit.start.presenter.fragment.CodeFragment
import kit.start.presenter.fragment.ListFragment
import kit.start.presenter.fragment.StatsFragment
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(RepositoryModule::class, AppModule::class, NetworkModule::class, ApiModule::class))
interface AppComponent {

    fun inject(listFragment: ListFragment)

    fun inject(codeFragment: CodeFragment)

    fun inject(statsFragment: StatsFragment)

    fun inject(analyticsMonthController: AnalyticsMonthController)

    companion object Factory {
        fun create(app: Application) = DaggerAppComponent.builder().
                appModule(AppModule(app)).
                apiModule(ApiModule("http://home.totruok.ru:39950/")).
                networkModule(NetworkModule()).
                repositoryModule(RepositoryModule()).
                build()
    }
}
