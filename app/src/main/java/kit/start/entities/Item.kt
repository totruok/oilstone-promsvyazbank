package kit.start.entities

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.JsonAdapter
import kit.start.entities.adapter.CurrencyAdapter

data class Item(
        @JsonAdapter(CurrencyAdapter::class)
        val price: String = "",
        val name: String = "",
        val description: String = "",
        val id: Int,
        val url: String = ""
) : Parcelable {
    constructor(source: Parcel) : this(
            source.readString(),
            source.readString(),
            source.readString(),
            source.readInt(),
            source.readString()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(price)
        writeString(name)
        writeString(description)
        writeInt(id)
        writeString(url)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<Item> = object : Parcelable.Creator<Item> {
            override fun createFromParcel(source: Parcel): Item = Item(source)
            override fun newArray(size: Int): Array<Item?> = arrayOfNulls(size)
        }
    }
}
