package kit.start.entities

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.JsonAdapter
import kit.start.entities.adapter.CurrencyAdapter

data class ShopCart(
        @JsonAdapter(CurrencyAdapter::class)
        val summ_cost: String = "",
        val name: String = "",
        val limit_exceeded: Boolean = false,
        val percentage_of_the_total_check_cost: Double = 0.0
) : Parcelable {
    constructor(source: Parcel) : this(
            source.readString(),
            source.readString(),
            1 == source.readInt(),
            source.readDouble()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(summ_cost)
        writeString(name)
        writeInt((if (limit_exceeded) 1 else 0))
        writeDouble(percentage_of_the_total_check_cost)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<ShopCart> = object : Parcelable.Creator<ShopCart> {
            override fun createFromParcel(source: Parcel): ShopCart = ShopCart(source)
            override fun newArray(size: Int): Array<ShopCart?> = arrayOfNulls(size)
        }
    }
}
