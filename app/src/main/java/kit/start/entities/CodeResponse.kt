package kit.start.entities

import android.os.Parcel
import android.os.Parcelable

data class CodeResponse(
        val shop_cart: List<ShopCart>,
        val items: List<Item>,
        val success: Boolean
) : Parcelable {
    constructor(source: Parcel) : this(
            ArrayList<ShopCart>().apply { source.readList(this, ShopCart::class.java.classLoader) },
            ArrayList<Item>().apply { source.readList(this, Item::class.java.classLoader) },
            1 == source.readInt()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeList(shop_cart)
        writeList(items)
        writeInt((if (success) 1 else 0))
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<CodeResponse> = object : Parcelable.Creator<CodeResponse> {
            override fun createFromParcel(source: Parcel): CodeResponse = CodeResponse(source)
            override fun newArray(size: Int): Array<CodeResponse?> = arrayOfNulls(size)
        }
    }
}
