package kit.start.entities

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.JsonAdapter
import kit.start.entities.adapter.CurrencyAdapter

data class CatsItem(
        val over_limit_at_the_end: Boolean = false,
        @JsonAdapter(CurrencyAdapter::class)
        val spent: String = "",
        val name: String = "",
        @JsonAdapter(CurrencyAdapter::class)
        val limit: String = "",
        @JsonAdapter(CurrencyAdapter::class)
        val forecast: String = "",
        val days_points: List<Float>,
        val is_total: Boolean = false

) : Parcelable {
    constructor(source: Parcel) : this(
            1 == source.readInt(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            ArrayList<Float>().apply { source.readList(this, Float::class.java.classLoader) },
            1 == source.readInt()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeInt((if (over_limit_at_the_end) 1 else 0))
        writeString(spent)
        writeString(name)
        writeString(limit)
        writeString(forecast)
        writeList(days_points)
        writeInt((if (is_total) 1 else 0))
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<CatsItem> = object : Parcelable.Creator<CatsItem> {
            override fun createFromParcel(source: Parcel): CatsItem = CatsItem(source)
            override fun newArray(size: Int): Array<CatsItem?> = arrayOfNulls(size)
        }
    }
}

