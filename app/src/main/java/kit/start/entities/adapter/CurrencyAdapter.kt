package kit.start.entities.adapter

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import java.lang.reflect.Type
import java.text.NumberFormat
import java.util.*


class CurrencyAdapter: JsonDeserializer<String> {
    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): String {
        if(json?.isJsonPrimitive != null) {
            val value = json.asLong

            val format = NumberFormat.getCurrencyInstance(Locale.getDefault())
            format.currency = Currency.getInstance("RUB")
            return format.format(value / 100f)
        }
        return "0"

    }
}