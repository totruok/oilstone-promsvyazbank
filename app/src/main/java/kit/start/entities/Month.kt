package kit.start.entities

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.JsonAdapter
import kit.start.entities.adapter.CurrencyAdapter

data class Month(
        @JsonAdapter(CurrencyAdapter::class)
        val totalSpent: String = "",
        val cats: List<CatsItem>,
        val currentDay: Int = 0
) : Parcelable {
    constructor(source: Parcel) : this(
            source.readString(),
            source.createTypedArrayList(CatsItem.CREATOR),
            source.readInt()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(totalSpent)
        writeTypedList(cats)
        writeInt(currentDay)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<Month> = object : Parcelable.Creator<Month> {
            override fun createFromParcel(source: Parcel): Month = Month(source)
            override fun newArray(size: Int): Array<Month?> = arrayOfNulls(size)
        }
    }
}
