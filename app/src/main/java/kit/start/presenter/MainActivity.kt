package kit.start.presenter

import android.app.Fragment
import android.os.Bundle
import android.app.NotificationManager
import android.app.NotificationChannel
import android.os.Build
import kit.start.R
import kit.start.entities.CodeResponse
import kit.start.entities.Item
import kit.start.entities.ShopCart
import kit.start.presenter.base.BaseActivity
import kit.start.presenter.fragment.AnalyticsFragment
import kit.start.presenter.fragment.CodeFragment
import kit.start.presenter.fragment.MonthFragment
import kit.start.presenter.fragment.StatsFragment


class MainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            val channelId = getString(R.string.notification_channel_id)
            val channelName = getString(R.string.notification_channel_name)
            val notificationManager = getSystemService(NotificationManager::class.java)
            notificationManager!!.createNotificationChannel(NotificationChannel(channelId,
                    channelName, NotificationManager.IMPORTANCE_LOW))
        }

        if (savedInstanceState == null) {
            openCode()
        }
    }

    override fun onBackPressed() {
        if(currentFragment() is AnalyticsFragment) {
            debounceAction {
                replace(CodeFragment())
            }
        } else {
            super.onBackPressed()
        }

    }

    private fun openCode() {
        debounceAction {
            add(CodeFragment())
        }

    }

    fun openAnalytics(analytics: CodeResponse) {
        debounceAction {
            val fragment = AnalyticsFragment()
            fragment.arguments = Bundle().apply {
                putParcelable("analytics", analytics)
            }
            replace(fragment)
        }
    }

    fun openStats() {
        debounceAction {
            addBackStack(StatsFragment(), "stats")
        }
    }




}
