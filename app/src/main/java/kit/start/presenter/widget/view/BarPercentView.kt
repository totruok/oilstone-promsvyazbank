package kit.start.presenter.widget.view

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.support.annotation.ColorRes
import android.support.v4.content.ContextCompat
import android.util.AttributeSet
import android.widget.FrameLayout

class BarPercentView @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    init {
        setWillNotDraw(false)
    }

    private var paint: Paint? = null
    var percent = 0f
        set(value) {
            field = value


            invalidate()
        }

    fun color(@ColorRes color: Int) {
        paint = Paint()
        paint?.color = ContextCompat.getColor(context, color)
        invalidate()
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        if (paint != null) {
            drawLineProgress(canvas)
        }
    }

    private fun drawLineProgress(canvas: Canvas) {
        val width = measuredWidth - paddingLeft - paddingRight

        canvas.drawRect(paddingLeft.toFloat(), paddingBottom.toFloat(), width * percent, (height - paddingTop).toFloat(), paint)
    }
}
