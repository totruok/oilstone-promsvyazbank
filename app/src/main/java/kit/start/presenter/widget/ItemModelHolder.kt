package kit.start.presenter.widget

import android.view.View
import android.widget.TextView
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyHolder
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import kit.start.R

@EpoxyModelClass(layout = R.layout.model_item)
abstract class ItemModelHolder : EpoxyModelWithHolder<ItemModelHolder.ItemHolder>() {

    @EpoxyAttribute
    var number: Int = 0
    @EpoxyAttribute
    var name: String = ""
    @EpoxyAttribute
    var price: String = ""

    override fun bind(holder: ItemHolder) {
        super.bind(holder)
        val context = holder.itemView.context

        holder.number.text = context.getString(R.string.item_n, number)
        holder.name.text = name
        holder.price.text = price

    }

    override fun unbind(holder: ItemHolder) {
        super.unbind(holder)

        holder.number.text = ""
        holder.name.text = ""
        holder.price.text = ""
    }

    class ItemHolder : EpoxyHolder() {
        lateinit var itemView: View
        lateinit var number: TextView
        lateinit var name: TextView
        lateinit var price: TextView

        override fun bindView(itemView: View) {
            this.itemView = itemView
            number = itemView.findViewById(R.id.number)
            name = itemView.findViewById(R.id.name)
            price = itemView.findViewById(R.id.price)
        }
    }
}