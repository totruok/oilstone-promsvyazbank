package kit.start.presenter.widget;

import android.view.View
import android.widget.TextView
import com.airbnb.epoxy.EpoxyAttribute;
import com.airbnb.epoxy.EpoxyHolder
import com.airbnb.epoxy.EpoxyModelClass;
import com.airbnb.epoxy.EpoxyModelWithHolder
import kit.start.R

@EpoxyModelClass(layout = R.layout.model_label)
abstract class LabelModelHolder : EpoxyModelWithHolder<LabelModelHolder.LabelHolder>() {

    @EpoxyAttribute
    var titleRes: Int = 0
    @EpoxyAttribute
    var subtitleRes: Int = 0

    override fun bind(holder: LabelHolder) {
        super.bind(holder)
        val context = holder.itemView.context

        if (titleRes != 0) {
            holder.title.text = context.getString(titleRes)
        }
        if (subtitleRes != 0) {
            holder.subtitle.text = context.getString(subtitleRes)
        }
    }

    override fun unbind(holder: LabelHolder) {
        super.unbind(holder)


        holder.title.text = ""
        holder.subtitle.text = ""
    }

    class LabelHolder : EpoxyHolder() {
        lateinit var itemView: View
        lateinit var title: TextView
        lateinit var subtitle: TextView

        override fun bindView(itemView: View) {
            this.itemView = itemView
            title = itemView.findViewById(R.id.title)
            subtitle = itemView.findViewById(R.id.subtitle)
        }
    }
}