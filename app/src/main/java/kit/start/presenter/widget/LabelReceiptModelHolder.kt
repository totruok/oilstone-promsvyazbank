package kit.start.presenter.widget

import android.view.View
import android.widget.TextView
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyHolder
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import kit.start.R

@EpoxyModelClass(layout = R.layout.model_label_receipt)
abstract class LabelReceiptModelHolder : EpoxyModelWithHolder<LabelReceiptModelHolder.LabelHolder>() {

    @EpoxyAttribute
    var titleRes: Int = 0

    override fun bind(holder: LabelHolder) {
        super.bind(holder)
        val context = holder.itemView.context

        if (titleRes != 0) {
            holder.title.text = context.getString(titleRes)
        }
    }

    override fun unbind(holder: LabelHolder) {
        super.unbind(holder)

        holder.title.text = ""
    }

    class LabelHolder : EpoxyHolder() {
        lateinit var itemView: View
        lateinit var title: TextView

        override fun bindView(itemView: View) {
            this.itemView = itemView
            title = itemView.findViewById(R.id.title)
        }
    }
}