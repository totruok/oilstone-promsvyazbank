package kit.start.presenter.widget

import android.view.View
import android.widget.TextView
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyHolder
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import kit.start.R
import kit.start.presenter.widget.view.BarPercentView

@EpoxyModelClass(layout = R.layout.model_category)
abstract class CategoryModelHolder : EpoxyModelWithHolder<CategoryModelHolder.CategoryHolder>() {

    @EpoxyAttribute
    var name: String = ""
    @EpoxyAttribute
    var price: String = ""
    @EpoxyAttribute
    var limited: Boolean = false

    @EpoxyAttribute
    var percent: Float = 0f

    override fun bind(holder: CategoryHolder) {
        super.bind(holder)
        val context = holder.itemView.context

        holder.name.text = name
        holder.price.text = price
        if (limited) {
            holder.bar.color(R.color.international_orange)
            holder.limited.visibility = View.VISIBLE
        } else {
            holder.limited.visibility = View.INVISIBLE
            holder.bar.color(R.color.white_20)
        }
        holder.bar.percent = percent

    }

    override fun unbind(holder: CategoryHolder) {
        super.unbind(holder)

        holder.name.text = ""
        holder.price.text = ""

    }

    class CategoryHolder : EpoxyHolder() {
        lateinit var itemView: View
        lateinit var name: TextView
        lateinit var price: TextView
        lateinit var bar: BarPercentView
        lateinit var limited: TextView

        override fun bindView(itemView: View) {
            this.itemView = itemView
            name = itemView.findViewById(R.id.name)
            price = itemView.findViewById(R.id.price)
            bar = itemView.findViewById(R.id.bar)
            limited = itemView.findViewById(R.id.limited)
        }
    }
}