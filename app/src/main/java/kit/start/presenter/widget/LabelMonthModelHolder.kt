package kit.start.presenter.widget

import android.view.View
import android.widget.TextView
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyHolder
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import kit.start.R
import kit.start.presenter.model.StatTitle

@EpoxyModelClass(layout = R.layout.model_label_month)
abstract class LabelMonthModelHolder : EpoxyModelWithHolder<LabelMonthModelHolder.LabelHolder>() {

    @EpoxyAttribute
    var statTitle: StatTitle? = null

    override fun bind(holder: LabelHolder) {
        super.bind(holder)
        val context = holder.itemView.context

        if (statTitle != null) {
            holder.title.text = statTitle?.month?:""
            @Suppress("DEPRECATION")
            val left = if (statTitle!!.left ) {
                context.resources.getDrawable(R.drawable.ic_keyboard_arrow_left_white_24dp)
            } else {
                null
            }
            @Suppress("DEPRECATION")
            val right = if (statTitle!!.right) {
                context.resources.getDrawable(R.drawable.ic_keyboard_arrow_right_white_24dp)
            } else {
                null
            }
            holder.title.setCompoundDrawablesWithIntrinsicBounds(left, null, right, null)
        }



    }

    override fun unbind(holder: LabelHolder) {
        super.unbind(holder)

        holder.title.text = ""
        holder.title.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null)
    }

    class LabelHolder : EpoxyHolder() {
        lateinit var itemView: View
        lateinit var title: TextView

        override fun bindView(itemView: View) {
            this.itemView = itemView
            title = itemView.findViewById(R.id.title)
        }
    }
}