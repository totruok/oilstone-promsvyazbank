package kit.start.presenter.widget

import android.content.Context
import android.support.v4.content.ContextCompat
import android.util.Log
import android.view.View
import android.widget.TextView
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyHolder
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.github.mikephil.charting.animation.Easing
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.Description
import com.github.mikephil.charting.data.DataSet
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import kit.start.R
import kit.start.presenter.widget.view.BarPercentView
import kit.start.R.id.chart
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis.AxisDependency
import kit.start.R.id.chart
import com.github.mikephil.charting.components.YAxis





@EpoxyModelClass(layout = R.layout.model_stat)
abstract class StatModelHolder : EpoxyModelWithHolder<StatModelHolder.CategoryHolder>() {

    @EpoxyAttribute
    var name: String = ""
    @EpoxyAttribute
    var spent: String = ""
    @EpoxyAttribute
    var limit: String = ""
    @EpoxyAttribute
    var forecast: String = ""
    @EpoxyAttribute
    var limited: Boolean = false
    @EpoxyAttribute
    var summary: Boolean = false
    @EpoxyAttribute
    var top: Boolean = false
    @EpoxyAttribute
    var percent: Float = 0f
    @EpoxyAttribute(EpoxyAttribute.Option.DoNotHash)
    var points: List<Float>? = null

    private val TAG = this.javaClass.simpleName!!


    override fun bind( holder: CategoryHolder) {
        super.bind(holder)
        val context = holder.itemView.context


        if (points?.isNotEmpty() != null) {
            initPieChart(holder.lineChart, points!!)
            holder.itemView.setOnClickListener {
                if (holder.chartWrapper.visibility == View.VISIBLE) {
                    holder.chartWrapper.visibility = View.GONE

                } else {
                    holder.chartWrapper.visibility = View.VISIBLE
                    holder.chartWrapper.postDelayed({
                        holder?.lineChart?.animateXY(400, 400)
                    },50)

                }
            }
        }

        holder.name.text = name
        holder.limit.text = limit
        holder.spent.text = spent
        holder.forecast.text = forecast

        var textColor = ContextCompat.getColor(context, android.R.color.white)

        if (top) {
            holder.bar.color(android.R.color.transparent)
            holder.name.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null)
        } else {
            @Suppress("DEPRECATION")
            holder.name.setCompoundDrawablesWithIntrinsicBounds(null, null, context.resources.getDrawable(R.drawable.ic_keyboard_arrow_right_white_24dp), null)
            if (summary) {
                holder.bar.color(android.R.color.white)
                textColor = ContextCompat.getColor(context, android.R.color.black)
            } else {
                if (limited) {
                    holder.bar.color(R.color.international_orange)
                } else {
                    holder.bar.color(R.color.white_20)
                }
            }
        }
        holder.name.setTextColor(textColor)
        holder.limit.setTextColor(textColor)
        holder.spent.setTextColor(textColor)
        holder.forecast.setTextColor(textColor)

        holder.bar.percent = 1f

    }

    private fun initPieChart(chart: LineChart, ps: List<Float>){

        val entries = mutableListOf<Entry>()
        //val dataSet = LineDataSet()
        for (i in 0 until ps.size) {
            // turn your data into Entry objects
            entries.add(Entry(i.toFloat(), ps[i] /100))
        }
        Log.d(TAG, entries.toString())
        val dataSet = LineDataSet(entries, "Label")
        dataSet.setDrawValues(false)
        dataSet.setDrawCircles(false)
        dataSet.setDrawFilled(true)
        dataSet.color = ContextCompat.getColor(chart.context, android.R.color.white)
        dataSet.fillColor = ContextCompat.getColor(chart.context, android.R.color.white)

        chart.setTouchEnabled(false)
        chart.data = LineData(dataSet)
        val chartX = chart.xAxis

        chartX.labelCount = 7
        chartX.setDrawLabels(true)
        chartX.position = XAxis.XAxisPosition.BOTTOM
        chartX.axisLineColor = ContextCompat.getColor(chart.context, R.color.white_50)
        chartX.textColor = ContextCompat.getColor(chart.context, android.R.color.white)
        chartX.setDrawGridLines(false)
        chart.axisRight.isEnabled = false
        chart.legend.isEnabled = false
        chart.description.text = ""

        val left = chart.axisLeft
        left.setDrawZeroLine(true) // draw a zero line
        left.zeroLineColor = ContextCompat.getColor(chart.context, R.color.international_orange)
        left.gridColor = ContextCompat.getColor(chart.context, R.color.white_50)
        left.setDrawAxisLine(false)
        left.zeroLineWidth = chart.context.resources.getDimension(R.dimen.zeroLine)
        left.textColor = ContextCompat.getColor(chart.context, android.R.color.white)

        chart.invalidate()
    }

    override fun unbind(holder: CategoryHolder) {
        super.unbind(holder)

        holder.name.text = ""
        holder.spent.text = ""
        holder.limit.text = ""
        holder.forecast.text = ""
    }

    class CategoryHolder : EpoxyHolder() {
        lateinit var itemView: View
        lateinit var name: TextView
        lateinit var limit: TextView
        lateinit var spent: TextView
        lateinit var forecast: TextView
        lateinit var bar: BarPercentView
        lateinit var chartWrapper: View
        lateinit var lineChart: LineChart

        override fun bindView(itemView: View) {
            this.itemView = itemView
            name = itemView.findViewById(R.id.name)
            limit = itemView.findViewById(R.id.limit)
            spent = itemView.findViewById(R.id.spent)
            forecast = itemView.findViewById(R.id.forecast)
            lineChart = itemView.findViewById(R.id.chart)
            bar = itemView.findViewById(R.id.bar)
            chartWrapper = itemView.findViewById(R.id.chart_wrapper)
        }
    }
}