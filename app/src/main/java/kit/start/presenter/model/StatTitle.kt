package kit.start.presenter.model

import android.os.Parcel
import android.os.Parcelable

data class StatTitle(val left: Boolean, val right: Boolean, val month: String) : Parcelable {
    constructor(source: Parcel) : this(
            1 == source.readInt(),
            1 == source.readInt(),
            source.readString()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeInt((if (left) 1 else 0))
        writeInt((if (right) 1 else 0))
        writeString(month)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<StatTitle> = object : Parcelable.Creator<StatTitle> {
            override fun createFromParcel(source: Parcel): StatTitle = StatTitle(source)
            override fun newArray(size: Int): Array<StatTitle?> = arrayOfNulls(size)
        }
    }
}

