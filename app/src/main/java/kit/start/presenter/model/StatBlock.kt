package kit.start.presenter.model

import android.os.Parcel
import android.os.Parcelable
import kit.start.entities.Month

data class StatBlock(
        val title: StatTitle,
        val month: Month
) : Parcelable {
    constructor(source: Parcel) : this(
            source.readParcelable<StatTitle>(StatTitle::class.java.classLoader),
            source.readParcelable<Month>(Month::class.java.classLoader)
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeParcelable(title, 0)
        writeParcelable(month, 0)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<StatBlock> = object : Parcelable.Creator<StatBlock> {
            override fun createFromParcel(source: Parcel): StatBlock = StatBlock(source)
            override fun newArray(size: Int): Array<StatBlock?> = arrayOfNulls(size)
        }
    }
}

