package kit.start.presenter.fragment

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import kit.start.R
import kit.start.entities.Month
import kit.start.presenter.model.StatBlock
import kit.start.presenter.model.StatTitle

class StatsPagerAdapter(var months: List<Month>, val context: Context, fm: FragmentManager?) : FragmentPagerAdapter(fm) {

    init {
        months = months.reversed()
    }
    private val NUM_ITEMS = months.size

    override fun getItem(position: Int): Fragment = MonthFragment.instance(createStatBlock(position, months[position]))

    private fun createStatBlock(position: Int, month: Month): StatBlock {
        var left = true
        if (position == 0) {
            left = false
        }

        var right = true
        if (position == months.size - 1) {
            right = false
        }

        return StatBlock(StatTitle(left, right, getPageTitle(position).toString()), month)

    }
    override fun getPageTitle(position: Int): CharSequence = when (position) {
        8 -> context.getString(R.string.month_0)
        7 -> context.getString(R.string.month_1)
        6 -> context.getString(R.string.month_2)
        5 -> context.getString(R.string.month_3)
        4 -> context.getString(R.string.month_4)
        3 -> context.getString(R.string.month_5)
        2 -> context.getString(R.string.month_6)
        1 -> context.getString(R.string.month_7)
        0 -> context.getString(R.string.month_8)
        11 -> context.getString(R.string.month_9)
        10 -> context.getString(R.string.month_10)
        9 -> context.getString(R.string.month_11)
        else -> {
            throw IllegalArgumentException()
        }
    }

    override fun getCount(): Int {
        return NUM_ITEMS
    }


}

