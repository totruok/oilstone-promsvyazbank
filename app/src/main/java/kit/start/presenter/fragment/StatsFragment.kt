package kit.start.presenter.fragment

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.widget.SwipeRefreshLayout
import android.view.View
import kit.start.App
import kit.start.R
import kit.start.data.Repository
import kit.start.presenter.annotation.Back
import kit.start.presenter.annotation.Layout
import kit.start.presenter.annotation.Title
import kit.start.presenter.base.BaseFragment
import kit.start.presenter.controller.AnalyticsMonthCallbacks
import kotlinx.android.synthetic.main.fragment_stats.*
import kotlinx.android.synthetic.main.layout_recycler.*
import javax.inject.Inject

@Back
@Title(R.string.title_analytics_month)
@Layout(R.layout.fragment_stats)
class StatsFragment() : BaseFragment(), AnalyticsMonthCallbacks, SwipeRefreshLayout.OnRefreshListener {

    @Inject
    lateinit var repository: Repository

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        App.appComponent.inject(this)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        refreshLayout.setColorSchemeResources(
                R.color.accent,
                R.color.international_orange)

        getData()
    }

    override fun onRefresh() {
        getData()
    }

    private fun getData() {
        refreshLayout.isRefreshing = true
        subscribe(repository.months()
                .subscribe({
                    viewPager.adapter = StatsPagerAdapter(it, context, childFragmentManager)
                    viewPager.visibility = View.VISIBLE
                    viewPager.currentItem = it.size
                },
                        { t -> t.printStackTrace()
                            showError(getString(R.string.error_loading_stats))
                        }))
    }

    private fun stopRefresh(){
        refreshLayout.postDelayed({
            refreshLayout?.isRefreshing = false
        }, 100)
    }

    private fun showError(message: String) {
        stopRefresh()

        val hasData = viewPager.adapter != null && recyclerView.adapter.itemCount > 0
        val snackbar = Snackbar.make(view!!, message, if (hasData) Snackbar.LENGTH_LONG else Snackbar.LENGTH_INDEFINITE)
        snackbar.setAction(getString(R.string.button_retry), {
            refreshLayout.isRefreshing = true
            getData()
        })

        snackbar.show()
    }
}