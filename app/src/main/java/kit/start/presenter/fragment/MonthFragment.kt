package kit.start.presenter.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.View

import kit.start.R
import kit.start.presenter.annotation.Layout
import kit.start.presenter.controller.AnalyticsMonthCallbacks
import kit.start.presenter.controller.AnalyticsMonthController
import kit.start.presenter.model.StatBlock
import kotlinx.android.synthetic.main.layout_recycler.*

@Layout(R.layout.fragment_recycler_month)
class MonthFragment : ListFragment(), AnalyticsMonthCallbacks {

    private val controller = AnalyticsMonthController(this, recycledViewPool)

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recyclerView.adapter = controller.adapter
        val statBlock: StatBlock = arguments.getParcelable("month")
        controller.setData(statBlock)
    }

    override fun openMonthStats() {

    }

    companion object {
        fun instance(statBlock: StatBlock): Fragment {
            val fragment = MonthFragment()
            fragment.arguments = Bundle().apply {
                putParcelable("month", statBlock)
            }
            return fragment
        }

    }
}