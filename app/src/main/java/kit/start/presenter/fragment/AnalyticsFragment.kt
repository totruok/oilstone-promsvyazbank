package kit.start.presenter.fragment

import android.os.Bundle
import android.view.View
import com.guidewithme.presenter.controller.AnalyticsCallbacks
import com.guidewithme.presenter.controller.AnalyticsController
import kit.start.R
import kit.start.entities.CodeResponse
import kit.start.presenter.MainActivity
import kit.start.presenter.annotation.Layout
import kit.start.presenter.annotation.Title
import kotlinx.android.synthetic.main.layout_recycler.*

@Title(R.string.title_analytics)
@Layout(R.layout.fragment_recycler_analytics)
class AnalyticsFragment: ListFragment(), AnalyticsCallbacks {

    private val controller = AnalyticsController(this, recycledViewPool)

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recyclerView.adapter = controller.adapter
        val codeResponse:CodeResponse = arguments.getParcelable("analytics")
        controller.setData(codeResponse)
    }

    override fun openMonthStats() {
        (activity as MainActivity).openStats()
    }
}
