package kit.start.presenter.fragment

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import com.guidewithme.presenter.controller.AnalyticsCallbacks
import kit.start.presenter.base.BaseFragment
import kotlinx.android.synthetic.main.layout_recycler.*

abstract class ListFragment : BaseFragment(), AnalyticsCallbacks {

    protected val recycledViewPool = RecyclerView.RecycledViewPool()

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recyclerView.recycledViewPool = recycledViewPool
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.setHasFixedSize(true)
    }
}
