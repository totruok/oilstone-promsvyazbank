package kit.start.presenter.fragment

import android.os.Bundle
import android.support.design.widget.BaseTransientBottomBar
import android.support.design.widget.Snackbar
import android.util.Log
import android.util.SparseArray
import android.view.View
import com.google.android.gms.samples.vision.barcodereader.BarcodeGraphic
import com.google.android.gms.vision.barcode.Barcode
import kit.start.R
import kit.start.presenter.annotation.Layout
import kit.start.presenter.base.BaseFragment
import xyz.belvi.mobilevisionbarcodescanner.BarcodeRetriever
import com.google.android.gms.samples.vision.barcodereader.BarcodeCapture
import kit.start.App
import kit.start.data.Repository
import kit.start.presenter.MainActivity
import kotlinx.android.synthetic.main.fragment_code.*
import javax.inject.Inject
import android.view.animation.AccelerateInterpolator
import android.support.v4.view.ViewCompat.animate
import android.view.animation.DecelerateInterpolator




@Layout(R.layout.fragment_code)
class CodeFragment: BaseFragment(), BarcodeRetriever {

    private val TAG = this.javaClass.simpleName!!

    private lateinit var barcodeCapture:BarcodeCapture

    @Inject
    lateinit var repository: Repository

    var success: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        App.appComponent.inject(this)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        animation_view.progress = 0f

        barcodeCapture = childFragmentManager.findFragmentById(R.id.barcode) as BarcodeCapture
        barcodeCapture.setRetrieval(this)
    }

    override fun onRetrieved(barcode: Barcode) {
        if (!success) {
            success = true

            activity.runOnUiThread {
                shadow.animate().alpha(1f)
                        .setDuration(500L)
                        .setInterpolator(DecelerateInterpolator())
                       .start()
                animation_view.playAnimation()
                onSuccess(barcode)
            }
        }
    }


    private fun onSuccess(barcode: Barcode) {
        qrTitle.text = getString(R.string.qr_success)
        qrSubtitle.text = getString(R.string.qr_soon)

        barcodeCapture.retrieveCamera().stopPreview()

        Log.e(TAG, barcode.displayValue)
        subscribe(repository.send(barcode.displayValue).subscribe({
            view?.handler?.postDelayed({
                (activity as MainActivity)?.openAnalytics(it)
                //onError(getString(R.string.qr_server_error))
            }, 1000)
        },{
            it.printStackTrace()
            onError(getString(R.string.qr_server_error))
        }))
    }

    private fun onError(message: String) {

        Snackbar.make(coordinator, message, Snackbar.LENGTH_INDEFINITE).addCallback(object : BaseTransientBottomBar.BaseCallback<Snackbar>(){
            override fun onShown(transientBottomBar: Snackbar?) {
                super.onShown(transientBottomBar)
            }

            override fun onDismissed(transientBottomBar: Snackbar?, event: Int) {
                super.onDismissed(transientBottomBar, event)
                qrTitle?.text = getString(R.string.qr_hint)
                qrSubtitle?.text = getString(R.string.qr_hint_why)
                animation_view?.progress = 0f
                shadow?.animate()?.alpha(0f)
                        ?.setDuration(500L)
                        ?.setInterpolator(AccelerateInterpolator())
                        ?.start()

                shadow?.visibility = View.GONE
                barcodeCapture?.refresh()
                barcodeCapture?.retrieveCamera().startPreview()
                success = false
            }
        }).setAction(android.R.string.ok, {
        }).show()
    }

    override fun onRetrievedMultiple(barcode: Barcode?, graphics: MutableList<BarcodeGraphic>?) {

    }

    override fun onBitmapScanned(codes: SparseArray<Barcode>?) {
        if (codes != null) {
            (0 until codes.size())
                    .map { codes.valueAt(it) }
                    .forEach { Log.e("value", it.displayValue) }
        }
    }

    override fun onRetrievedFailed(error: String?) {

    }

    override fun onPermissionRequestDenied() {

    }
}
