package kit.start.presenter.controller

import android.content.Context
import android.support.v7.widget.RecyclerView
import com.airbnb.epoxy.TypedEpoxyController
import com.guidewithme.presenter.controller.AnalyticsCallbacks
import kit.start.App
import kit.start.R
import kit.start.presenter.model.StatBlock
import kit.start.presenter.widget.labelMonthModelHolder
import kit.start.presenter.widget.statModelHolder
import javax.inject.Inject

class AnalyticsMonthController(val callbacks: AnalyticsCallbacks, val recycledViewPool: RecyclerView.RecycledViewPool) : TypedEpoxyController<StatBlock>() {

    @Inject
    lateinit var context: Context

    private val TAG = this.javaClass.simpleName!!
    init {
        setDebugLoggingEnabled(true)
        App.appComponent.inject(this)
    }

    override fun buildModels(model: StatBlock) {

        labelMonthModelHolder {
            id("monthTitle")
            statTitle(model.title)
        }

        statModelHolder {
            id("monthTop")
            name(context.getString(R.string.month_item_name))
            spent(context.getString(R.string.month_item_spent))
            limit(context.getString(R.string.month_item_limit))
            forecast(context.getString(R.string.month_item_forecast))
            top(true)
        }
        for (i in 0 until model.month.cats.size) {
            val cat = model.month.cats[i]
            statModelHolder {
                id("cat$i")
                name(cat.name)
                spent(cat.spent)
                limit(cat.limit)
                summary(cat.is_total)
                forecast(cat.forecast)
                limited(cat.over_limit_at_the_end)
                points(if(i != 0) {
                    cat.days_points
                } else {
                    null
                })
            }
        }
    }
}