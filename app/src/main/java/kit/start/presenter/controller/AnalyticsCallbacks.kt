package com.guidewithme.presenter.controller;

interface AnalyticsCallbacks {
    fun openMonthStats()
}