package com.guidewithme.presenter.controller

import android.support.v7.widget.RecyclerView
import android.util.Log
import com.airbnb.epoxy.TypedEpoxyController
import kit.start.R
import kit.start.entities.CodeResponse
import kit.start.presenter.widget.*


class AnalyticsController(val callbacks: AnalyticsCallbacks, val recycledViewPool: RecyclerView.RecycledViewPool) : TypedEpoxyController<CodeResponse>() {


    private val TAG = this.javaClass.simpleName!!
    init {
        setDebugLoggingEnabled(true)
    }

    override fun buildModels(model: CodeResponse) {
        labelModelHolder {
            id("labelCategory")
            titleRes(R.string.category_title)
            subtitleRes(R.string.category_spent)
        }

        for (i in 0 until model.shop_cart.size) {
            val category = model.shop_cart[i]
            categoryModelHolder {
                id("category$i")
                name(category.name)
                price(category.summ_cost)
                percent(category.percentage_of_the_total_check_cost.toFloat() / 100)
                limited(category.limit_exceeded)
            }
        }
        buttonModelHolder {
            id("buttonCategory")
            textRes(R.string.button_month_stats)
            clickListener { _, _, _, _ ->
                callbacks.openMonthStats()
            }
        }

        labelReceiptModelHolder {
            id("labelItem")
            titleRes(R.string.item_title)
        }

        for (i in 0 until model.items.size) {
            val item = model.items[i]
            itemModelHolder {
                id("item$i")
                number(i+1)
                name(item.name)
                price(item.price)
            }
        }


    }
}
