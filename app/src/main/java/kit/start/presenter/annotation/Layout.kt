package kit.start.presenter.annotation

import android.support.annotation.LayoutRes

annotation class Layout(@LayoutRes val layout: Int)