package kit.start.presenter.annotation

import android.support.annotation.StringRes

annotation class Title(@StringRes val title: Int)