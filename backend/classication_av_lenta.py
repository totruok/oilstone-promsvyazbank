
# coding: utf-8

# ## Import

# In[1]:


import warnings
warnings.filterwarnings('ignore')

import pandas as pd
from scipy import stats
import numpy as np
from sklearn import svm, datasets
from sklearn.model_selection import train_test_split
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import classification_report
from sklearn.utils import shuffle
from sklearn.externals import joblib

# from pandas_keeper import col_metatypes, fvdf

import matplotlib.pyplot as plt


# ## Dataset

# In[2]:


av_df = pd.read_csv('av_lenta.csv')
av_df


# In[3]:


from sklearn.feature_extraction.text import TfidfVectorizer 


# In[4]:


vect = TfidfVectorizer()
X = vect.fit_transform(av_df['name'])
y = av_df['cat'].astype('category').cat.codes


# In[5]:


# shuffle the dataset
X, y = shuffle(X, y, random_state=0)

# Split the dataaset in two equal parts
X_train, X_test, y_train, y_test = train_test_split(
    X, y, test_size=0.20, random_state=0)


# ## GridSearch

# In[6]:


# Set the parameters by cross-validation
parameters = [
    {
        'kernel': ['rbf'],
        'gamma': [1e-4, 1e-3, 0.01, 0.1, 0.2, 0.5],
        'C': [1, 10, 100, 1000]
    },
    {
        'kernel': ['linear'], 
        'C': [1, 10, 100, 1000]
    }
]

print("# Tuning hyper-parameters")
print()


# In[ ]:


clf = GridSearchCV(svm.SVC(decision_function_shape='ovr'), parameters, cv=3, verbose=5, n_jobs=10)
clf.fit(X_train, y_train)


# In[ ]:


fn = 'gridsearch_av_lenta.pkl'
print('Save to {}'.format(fn))
joblib.dump(clf, fn)


# In[ ]:


print("Best parameters set found on development set:")
print()
print(clf.best_params_)
print()
print("Grid scores on training set:")
print()
means = clf.cv_results_['mean_test_score']
stds = clf.cv_results_['std_test_score']
for mean, std, params in zip(means, stds, clf.cv_results_['params']):
    print("%0.3f (+/-%0.03f) for %r"
          % (mean, std * 2, params))
print()


# ## Evaluation

# In[15]:


print("Detailed classification report:")
print()
print("The model is trained on the full development set.")
print("The scores are computed on the full evaluation set.")
print()
y_true, y_pred = y_test, clf.predict(X_test)
print(classification_report(y_true, y_pred))
print()


# In[29]:


cat = av_df['parent'].astype('category')
cat.cat.categories[62]
pd.concat([cat.cat.codes, cat], axis=1).head()


# In[ ]:





# In[37]:


# vector = vect.transform(['чай', 'свежезамороженный бекон', 'крупная рыба', 'шампунь 7 в 1 со вкусом ромашки'])
# vector
# cat.cat.categories[clf.predict(vector)]


# In[ ]:




