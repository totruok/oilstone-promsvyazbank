from flask import Flask
import pickle
import json
from flask import request
import requests
from random import randint
import warnings
from random import randint
warnings.filterwarnings('ignore')

import pandas as pd
from scipy import stats
import numpy as np
from sklearn import svm, datasets
from sklearn.model_selection import train_test_split
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import classification_report
from sklearn.utils import shuffle
from sklearn.externals import joblib
from sklearn.feature_extraction.text import TfidfVectorizer
app = Flask(__name__)
BASE_API = 'https://checash.herokuapp.com/'

def get_bill(bill_dict):
    r = requests.post(
        '{}{}'.format(BASE_API, 'bill/'),
        data=(bill_dict),
    )
    return r.json()

cmd_minus = {'Бакалея': 'Бакалея',
 'Барбекю': 'Разное',
 'Вафли, пряники, печенье': 'Хлеб и кондитерские изделия',
 'Готовые блюда и полуфабрикаты': 'Готовые блюда и полуфабрикаты',
 'Детское питание': 'Детское питание',
 'Йогурты': 'Молочные продукты',
 'Консервированные продукты': 'Консервированные продукты',
 'Макароны, паста, крупы': 'Бакалея',
 'Молочные продукты, яйца': 'Молочные продукты',
 'Овощи, фрукты, ягоды': 'Овощи, фрукты, ягоды',
 'Полуфабрикаты, готовые блюда, выпечка': 'Готовые блюда и полуфабрикаты',
 'Птица': 'Птица',
 'Рыба и морепродукты': 'Рыба и морепродукты',
 'Рыбная гастрономия': 'Рыба и морепродукты',
 'Сладости. Мармелад. Нуга. Цукаты': 'Хлеб и кондитерские изделия',
 'Сыры': 'Молочные продукты',
 'Сыры и колбасы': 'Молочные продукты',
 'Хлеб и кондитерские изделия': 'Хлеб и кондитерские изделия',
 'Хлеб, булочки, лепешки': 'Хлеб и кондитерские изделия',
 'Чипсы, сухарики, снеки, попкорн': 'Хлеб и кондитерские изделия'}

categorys = {
    "Алкоголь": 100000,
    "Безалкогольные напитки Полуфабрикаты": 40000,
    "Мясо и птица": 100000,
    "Рыба и морепродукты": 100000,
    "Консервированные продукты": 100000,
    "Молочные продукты": 100000,
    "Кулинария": 100000,
    "Бытовая химия": 100000,
    "Техника для дома": 100000,
    "Канцтовары": 100000,
}

def get_stats(aushan_data):
    aush_names = [i['name'] for i in aushan_data]

    vector = vect.transform(aush_names)
    cats_predicted = list(cat.cat.categories[clf.predict(vector)])
    cat_hist = {}
    total_price = 0
    for i in range(len(cats_predicted)):
        #print (aushan_data[i]['name'], cats_predicted[i])
        if cmd_minus[cats_predicted[i]] not in cat_hist:
            cat_hist[cmd_minus[cats_predicted[i]]] = aushan_data[i]['price']
        else:
            cat_hist[cmd_minus[cats_predicted[i]]] += aushan_data[i]['price']
        total_price += cat_hist[cmd_minus[cats_predicted[i]]]

    res_hist_cat = []
    for _cat in list(cat_hist.keys()):
        _limit = False
        if randint(1,10) < 4:
            _limit = True
        _tmp_res_cat = {
            'name': _cat,
            'limit_exceeded': _limit,
            'percentage_of_the_total_check_cost': (cat_hist[_cat] / total_price) * 100,
            'summ_cost': cat_hist[_cat]
        }
        res_hist_cat.append(_tmp_res_cat)

    return res_hist_cat


@app.route("/get_months_stats", methods=['GET'])
def get_months_stats():
    return json.dumps(fake_statistics)


@app.route("/process_bill", methods=['POST'])
def process_bill():
    print(request.form['code'])
    # t=20170916T072002&s=520.00&fn=8710000100435452&i=29106&fp=3839738939&n=1
    bill_code = request.form['code']
    result = {
        'success': False
    }
    # проверялка что там ок данные
    try:
        bill_params = {d.split('=')[0]:d.split('=')[1] for d in bill_code.split('&')}
    except:
        result['details'] = "Can't parse bill code. Is it bill QR code?"
        return json.dumps(result)
    necessary_fields = set(['fn', 'fp', 'i', 'n', 's', 't'])
    bill_fields = set(bill_params.keys())
    if len(necessary_fields - bill_fields) + len(bill_fields - necessary_fields) != 0:
        result['details'] = "Not enough required fields"
        print(bill_fields)
        return json.dumps(result)

    bill_items = get_bill(bill_params)
    print(bill_items)
    # добавление чека пользователю
    # слип
    # достаём из пользователя список доступных чеков
    # если наш там есть достаем его
    try:
        _items = bill_items[0]['items']
    except:
        print('No items. mb bad qr code')
        result['details'] = "No items. mb bad qr code"
        return json.dumps(result)
    return json.dumps({
        'success': True,
        'items': _items,
        'shop_cart': get_stats(_items)
    })


fake_statistics = []
for month in range(9):
    month_fake_statistic = []
    _month_params = {
            'current_day': 30,
        }
    total_money_spent = 0
    total_forecast = 0
    total_spend = 0
    total_limit = 0
    for _category in list(categorys.keys()):
        cat_params = {}
        cat_params['name'] = _category
        cat_params['limit'] = categorys[_category]
        cat_params['is_total'] = False

        # over limit?

        if randint(0,10) < 4:
            cat_params['over_limit_at_the_end'] = True
            cat_params['spent'] = categorys[_category] + randint((categorys[_category]*0.1), (categorys[_category]*0.35*randint(1,5)))
        else:
            cat_params['over_limit_at_the_end'] = False
            cat_params['spent'] = categorys[_category] - randint((categorys[_category]*0.1), (categorys[_category]*0.35*randint(1,5)))
        cat_params["forecast"] = categorys[_category] - cat_params['spent']
        total_money_spent += cat_params['spent']
        total_forecast += cat_params['forecast']
        total_limit += categorys[_category]
        days_points = []
        total_points_sum = 0

#         if month == 0:
#             days_left = 16
#         else:
#             days_left = 30
        days_left = 30
        for i in range(days_left):
            middle_in_day = ((cat_params['spent']) - total_points_sum) / (days_left - i)
            #print (middle_in_day, (days_left - i), cat_params["forecast"], cat_params['spent'] - current_day_cost)
            print(middle_in_day, '/' , total_points_sum , cat_params['spent'], i, cat_params['over_limit_at_the_end'])
            if middle_in_day < 0:
                current_day_cost = 0
            else:
                current_day_cost = middle_in_day + (1 + -2*randint(0,1))*randint(1, int( middle_in_day*0.30*randint(1,1+ ((days_left - i + 1)//4)*(randint(1,3)))))

            if current_day_cost < 0:
                current_day_cost = 0
            total_points_sum += current_day_cost
            days_points.append(categorys[_category] - total_points_sum)
        cat_params['days_points'] = days_points
        month_fake_statistic.append(cat_params)
    _month_params['cats'] = sorted(month_fake_statistic, key=lambda k: k['forecast'])
    tmp_cat = {
        'name': "Итог",
        'limit': total_limit,
        'is_total': True,
        'spent': total_money_spent,
        'forecast': total_forecast,
        'over_limit_at_the_end': True if total_forecast>=0 else False,
    }
    _month_params['cats'][0:0] = [tmp_cat]
    fake_statistics.append(_month_params)

av_df = pd.read_csv('av_lenta.csv')
vect = TfidfVectorizer()
X = vect.fit_transform(av_df['name'])
y = av_df['level2'].astype('category').cat.codes
clf = joblib.load('gridsearch_av_lenta_level2.pkl')
cat = av_df['level2'].astype('category')


app.run(host='0.0.0.0', debug=True, port=39950)
